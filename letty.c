/* letty - a lightweight getty
 *
 * author: Frank Busse
 * license: SimPL-2.0
 * usage: letty <tty path> [gossip to indicate no terminal clearing]
 *
 *
 * targeted OS: Linux (letty uses non-Posix functions like vhangup())
 * tested compilers: clang, ccomp, gcc
 * tested libraries: dietlibc, glibc, musl
 *
 *
 * Bibliography
 * ------------
 *  [VT91] Standard ECMA-48, Control Functions for Coded Character Sets
 *     http://www.ecma-international.org/publications/files/ECMA-ST/Ecma-048.pdf
 *  [POS08] The Open Group Base Specifications Issue 7 (POSIX.1-2008)
 *     http://pubs.opengroup.org/onlinepubs/9699919799/
 *  [LSB10] Linux Standard Base Core Specification 4.1
 *     http://refspecs.linux-foundation.org/LSB_4.1.0/LSB-Core-generic/LSB-Core-generic/book1.html
 */



// -- configuration --


#ifndef LETTY_LOGIN_BASENAME
	#define LETTY_LOGIN_BASENAME "login"
#endif
#if !defined LETTY_LOGIN_NAME_MAX || LETTY_LOGIN_NAME_MAX < 1
	#define LETTY_LOGIN_NAME_MAX 256
#endif
#ifndef LETTY_LOGIN_PATH
	#define LETTY_LOGIN_PATH "/bin/login"
#endif
#ifndef LETTY_LOGIN_PROMPT
	#define LETTY_LOGIN_PROMPT "login: "
#endif
#ifndef LETTY_UTMPX_USER
	#define LETTY_UTMPX_USER "LOGIN"
#endif
#ifndef LETTY_WTMP_PATH
	#define LETTY_WTMP_PATH "/var/log/wtmp"
#endif
#ifndef LETTY_CLEAR_SEQUENCE
 	// [VT91/8.3.105: RIS - RESET TO INITIAL STATE]
	#define LETTY_CLEAR_SEQUENCE "\033c"
#endif
#ifndef LETTY_USE_UPDWTMP
	#define LETTY_USE_UPDWTMP 0
#endif



// -- feature tests --


// test for _Static_assert: gcc >=4.6, clang or C11
#ifndef __has_feature
	#define __has_feature(x) 0
#endif

#if !((defined __GNUC__  && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))) || \
     (defined __clang__ && __has_feature(c_static_assert)) || \
     (__STDC_VERSION__ == 201112L))
	#define _Static_assert(e,s)
#endif



// -- implementation --


#define _POSIX_C_SOURCE 200809L

#include <fcntl.h>
#include <signal.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <termios.h>
#include <time.h>
#include <unistd.h>
#include <utmpx.h>


extern int vhangup(void);
extern void *memccpy(void *, const void *, int,  size_t);



// ---- functions ----


#define write_out(s) write(STDOUT_FILENO, s, sizeof(s) - 1)
#define write_err(s) write(STDERR_FILENO, "Error: " s "\n", sizeof(s) + 7)
#define EXIT _exit(__LINE__)



/* initialize_tty(tty_path, pid, clear): opens tty_path as controlling terminal,
 * sets owner (root), group (root) and permissions (0600), redirects standard
 * streams and clears screen if clear is true.
 */
static inline void initialize_tty(const char * const tty_path, const pid_t pid, const bool clear) {
	int fd;


	// create a new session if we are not already session leader,
	// fails if we are process group leader but not session leader
	pid_t sid = getsid(0);
	if (sid != pid && setsid() == (pid_t) -1) { EXIT; }

	// detach controlling tty, Linux-specific
	if ((fd = open("/dev/tty", O_RDWR)) != -1) {
		if (ioctl(fd, TIOCNOTTY) == -1) { EXIT; }
		close(fd);
	}

	// open as controlling tty (first tty open, not specified by POSIX)
	if ((fd = open(tty_path, O_RDWR)) == -1) { EXIT; }
	if (isatty(fd) != 1) { EXIT; }

	// set user(root)/group(root, not tty) and permissions(0600)
	if (fchown(fd, (uid_t)0, (gid_t)0) == -1 || fchmod(fd, 0600) == -1) { EXIT; }

	// if controlling terminal is ct for other processes, steal it
	if (ioctl(fd, TIOCSCTTY, 1) == -1) { EXIT; }

	// close file descriptors, ignore return values
	close(fd); close(STDIN_FILENO); close(STDOUT_FILENO); close(STDERR_FILENO);

	// ignore SIGHUP from vhangup()
	struct sigaction sa;
	sa.sa_handler = SIG_IGN;
	sa.sa_flags   = 0;
	if (sigemptyset(&sa.sa_mask) == -1 || sigaction(SIGHUP, &sa, NULL) == -1) { EXIT; }

	// hangup
	if (vhangup() == -1) { EXIT; }

	// reset signal handling for SIGHUP
	sa.sa_handler = SIG_DFL;
	sa.sa_flags   = 0;
	if (sigemptyset(&sa.sa_mask) == -1 || sigaction(SIGHUP, &sa, NULL) == -1) { EXIT; }

	// open controlling terminal, redirect standard streams to terminal
	if ((fd = open(tty_path, O_RDWR)) == -1) { EXIT; }

	if (fd != STDIN_FILENO) {
		if (dup2(fd, STDIN_FILENO) == -1) { EXIT;
		} else { close(fd); } // ignore return value
	}

	if ((dup2(STDIN_FILENO, STDOUT_FILENO) == -1) || \
	    (dup2(STDIN_FILENO, STDERR_FILENO) == -1)) { EXIT; }

	// clear
	if (clear) {
		write_out(LETTY_CLEAR_SEQUENCE);
	}
}



/* set_utmp_entry(tty_path, entry, pid): finds the corresponding entry in the db
 * and sets ut_user (LETTY_UTMPX_USER), ut_line, ut_type (LOGIN_PROCESS) and tv.
 */
static inline bool set_utmp_entry(const char * const tty_path, struct utmpx * entry, const pid_t pid) {
	// select entry with our pid
	struct utmpx * e;
	while ((e = getutxent()) != NULL &&
	       (e->ut_type != INIT_PROCESS || e->ut_pid != pid)) {}

	if (e == NULL) { return false; }
	*entry = *e;


	// update values
	// - user: "implementation-defined name of the login process"
	//   [LSB10] null-terminated, truncated if need be
	_Static_assert (sizeof(entry->ut_user) > 0, "length of ut_user greater than 0");

	if (memccpy(entry->ut_user, LETTY_UTMPX_USER, '\0', sizeof(entry->ut_user) - 1) == NULL) {
		entry->ut_user[sizeof(entry->ut_user) - 1] = '\0';
	}

	// - line: "device name"
	//   [LSB10] null-terminated, without path, truncated if need be
	_Static_assert (sizeof(entry->ut_line) > 0, "length of ut_line greater than 0");

	const char * start = (start = strrchr(tty_path, '/')) == NULL ? tty_path : start + 1;
	if (memccpy(entry->ut_line, start, '\0', sizeof(entry->ut_line) - 1) == NULL) {
		entry->ut_line[sizeof(entry->ut_line) - 1] = '\0';
	}

	// - type of entry
	//   LOGIN_PROCESS: Identifies the session leader of a logged-in user.
	entry->ut_type = LOGIN_PROCESS;

	// - time
	time_t tv;
	if ((tv = time(NULL)) == (time_t)-1) { return false; }
	entry->ut_tv.tv_sec = tv;


	return true;
}



#if !LETTY_USE_UPDWTMP
/* write_wtmp(fd, entry): writes "entry" to the end of the file (fd), truncates
 * file to the old offset if writing failed
 */
static inline bool write_wtmp(const int fd, const struct utmpx entry) {
	// actual offset
	off_t offset = lseek(fd, 0, SEEK_END);
	if (offset == (off_t)-1) { return false; }

	// exclude huge utmpx structs ;)
	_Static_assert (sizeof(struct utmpx) <= ~(1 << (sizeof(ssize_t)*8 - 1)), "size of utmpx must fit ssize_t");

	// append entry, truncate to old offset in error case (race condition)
	ssize_t nbytes = write(fd, &entry, sizeof(struct utmpx));
	if (nbytes < (ssize_t) sizeof(struct utmpx)) {
		if ((nbytes != (ssize_t)-1) && (ftruncate(fd, offset) == -1)) {
			write_err("couldn't truncate wtmp"); }

		return false;
	}


	return true;
}
#endif



/* update_utmp_wtmp(tty_path, pid): resets utmp db, calls set_utmp_entry(),
 * updates and closes the db. If set_utmp_entry() was successful it also
 * opens the wtmp file, calls write_wtmp() and closes it. No file locking is
 * applied. Alternatively updwtmp() is called if letty was compiled with
 * LETTY_USE_UPDWTMP.
 */
static inline bool update_utmp_wtmp(const char * tty_path, const pid_t pid) {
	struct utmpx entry;
	bool set = false;
	bool ret = true;


	// reset, update and close utmp db
	setutxent();
	set = set_utmp_entry(tty_path, &entry, pid);
	ret = set ? pututxline(&entry) != NULL : false;
	endutxent();


	// set wtmp if set_utmp_entry() was successful
	if (set) {
#if !LETTY_USE_UPDWTMP
		// open wtmp, append entry and close wtmp
		int fd = open(LETTY_WTMP_PATH, O_WRONLY | O_SYNC);
		if (fd < 0) { return false; }
		ret = ret && write_wtmp(fd, entry);
		close(fd); // ignore return value
#else
		updwtmp(LETTY_WTMP_PATH, &entry);
#endif
	}


	return ret;
}



/* is_in_pfcs(c): checks whether a given character c is in the portable filename
 * character set [A-Za-z0-9._-]. See [POS08/Base Definitions: 3.276].
 */
static inline bool is_in_pfcs(char c) {
	return (((c >= 'a') && (c <= 'z')) || ((c >= '0') && (c <= '9')) ||
	        ((c >= 'A') && (c <= 'Z')) || (c == '.') || (c == '_') ||
	        (c == '-'));
}



/* read_login_name(): flushes STDIN, writes prompt, reads login name and returns
 * it if it is in [:pfcs:]+ and shorter than LETTY_LOGIN_PROMPT, otherwise exits.
 * See [POS08/Base Definitions: 8.3].
 */
static inline char * read_login_name() {
	static char login_name[LETTY_LOGIN_NAME_MAX + 1];


	// flush pending input
	tcflush(STDIN_FILENO, TCIFLUSH); // ignore return value

	// write prompt
	if (write_out(LETTY_LOGIN_PROMPT) != sizeof(LETTY_LOGIN_PROMPT) - 1) {
		EXIT; }

	// read characters one-by-one
	bool nl   = false;
	char c    = '\0';
	size_t i  = 0;
	ssize_t r = 1;

	while (!nl && i <= LETTY_LOGIN_NAME_MAX) {
		if ((r = read(STDIN_FILENO, &c, 1)) < (ssize_t)1) { EXIT; }

		// valid character
		if (is_in_pfcs(c)) {
			login_name[i] = c;
			++i;
		// newline
		} else if (c == '\n') {
			// terminate string
			login_name[i] = '\0';
			nl = true;
		// invalid character
		} else {
			write_err("invalid character");
			EXIT;
		}
	}

	if (!nl) {
		write_err("illegal length");
		EXIT;
	}


	return login_name;
}



/* main(argc, argv): initializes the tty, updates utmp/wtmp, reads login name
 * via initialize_tty(), update_utmp_wtmp() and read_login_name() and finally
 * execs login (LETTY_LOGIN_PATH -- login_name).
 */
int main(int argc, char * argv[]) {
	if (argc < 2) { EXIT; }

	pid_t pid = getpid();

	// initialize tty
	initialize_tty(argv[1], pid, argc <= 2);

	// update utmp and wtmp files
	if (!update_utmp_wtmp(argv[1], pid)) {
		write_err("updating utmp/wtmp"); }

	// read login name
	char * login_name = read_login_name();

	// execute login
	execl(LETTY_LOGIN_PATH, LETTY_LOGIN_BASENAME, "--", login_name, NULL);
	EXIT;
}
