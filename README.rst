Letty
=====

*Letty* is a (lightweight, limited, lousy...) implementation of a classical
getty_ without:

* autologin
* changing or setting environment variables
* delay
* fancy output (``/etc/issue``, hostname, time...)
* outdated features (``/etc/ttytype``)
* support for modems or physical terminals

It just initializes the terminal, prints the login prompt, reads the user name
and executes the ``login(1)`` program. Using non-Posix functions like
``vhangup()``, it is limited to Linux and similar systems.

.. _getty: http://en.wikipedia.org/wiki/Getty_%28Unix%29



Installation
------------

Compilation
...........

*Letty* comes as a single file (``letty.c``) without dependencies. Compile it
with your favorite C compiler or use ``make``, ``make install``. Edit
``Makefile`` according to your needs.


Configuration
.............

Configure via macros or ``Makefile``. If you don't need utmp/wtmp handling,
remove the corresponding sections from the file.

+--------------------------+---------------------------------+-------------------+----------------------------+
| name                     | Makefile                        | default           | description                |
+==========================+=================================+===================+============================+
| ``LETTY_LOGIN_NAME_MAX`` | ``getconf LOGIN_NAME_MAX``      | ``256``           | maximium login name length |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_LOGIN_BASENAME`` | ``basename`` of ``which login`` | ``login``         | login program              |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_LOGIN_PATH``     | ``which login``                 | ``/bin/login``    | path to login program      |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_LOGIN_PROMPT``   | ``"login: "``                   | ``"login: "``     | prompt                     |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_UTMPX_USER``     | ``"LOGIN"``                     | ``"LOGIN"``       | ``ut_user`` in utmpx entry |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_WTMP_PATH``      | ``/var/log/wtmp``               | ``/var/log/wtmp`` | path to wtmp file          |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_CLEAR_SEQUENCE`` | ``"\033c"``                     | ``"\033c"``       | terminal clear sequence    |
+--------------------------+---------------------------------+-------------------+----------------------------+
| ``LETTY_USE_UPDWTMP``    | ``0``                           | ``0``             | use libc' ``updwtmp()``    |
+--------------------------+---------------------------------+-------------------+----------------------------+



Usage
-----

The first argument given to *letty* is the full path of the device.
Arbitrary further arguments prevent terminal clearing.

::

    letty <tty path> [gossip to indicate no terminal clearing]

Gettys are usually invoked by ``init(8)`` and hence the configuration depends
on the init system. A common way is to edit ``/etc/inittab``:

::

    # TERMINALS
    c1:12345:respawn:/sbin/letty /dev/tty1 nc
    c2:2345:respawn:/sbin/letty /dev/tty2



Compilers and Libraries
-----------------------

*Letty* was tested with multiple C libraries (dietlibc_ 0.33, musl_ 0.9.3 (no
utmpx support), glibc_ 2.15) and compilers (clang_ 2.9, CompCert_ 1.11, gcc_
4.5.3). The binary sizes on the test system were:

+------------+-------------+---------+
| compiler   | binary size | linking |
+============+=============+=========+
| diet gcc   | 5460B       | static  |
+------------+-------------+---------+
| diet clang | 5948B       | static  |
+------------+-------------+---------+
| musl-gcc   | 8840B       | static  |
+------------+-------------+---------+
| ccomp, gcc | 9700B       | dynamic |
+------------+-------------+---------+
| clang      | 9704B       | dynamic |
+------------+-------------+---------+

.. _dietlibc: http://www.fefe.de/dietlibc/
.. _musl: http://www.etalabs.net/musl/
.. _glibc: http://www.gnu.org/software/libc/
.. _clang: http://clang.llvm.org/
.. _CompCert: http://compcert.inria.fr/
.. _gcc: http://gcc.gnu.org/



License
-------

SimPL-2.0_

.. _SimPL-2.0: http://opensource.org/licenses/simpl-2.0



Alternatives
------------

Gotty_ by the same author or agetty_, fgetty_, mingetty_, ngetty_, logind_,
among others.

.. _Gotty: https://bitbucket.org/251/gotty
.. _agetty: http://www.kernel.org/pub/linux/utils/util-linux/
.. _fgetty: http://www.fefe.de/fgetty/
.. _mingetty: http://sourceforge.net/projects/mingetty/
.. _ngetty: http://riemann.fmi.uni-sofia.bg/ngetty/
.. _logind: http://clientes.netvisao.pt/anbadeol/logind.html
