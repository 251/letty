LETTY_LOGIN_NAME_MAX = $(shell getconf LOGIN_NAME_MAX)
LETTY_LOGIN_BASENAME = "\"$(shell basename $(shell which login))"\"
LETTY_LOGIN_PATH = "\"$(shell which login)"\"
LETTY_LOGIN_PROMPT = "\"login: "\"
LETTY_UTMPX_USER = "\"LOGIN\""
LETTY_WTMP_PATH = "\"/var/log/wtmp\""
LETTY_CLEAR_SEQUENCE = "\"\033c"\"
LETTY_USE_UPDWTMP = 0

CC	= diet gcc
#CC	= diet clang
#CC	= musl-gcc
#CC	= gcc
#CC	= clang
#CC	= ccomp

DESTDIR =
MANDIR  = /usr/share/man/man8

CFLAGS = -D LETTY_LOGIN_NAME_MAX=$(LETTY_LOGIN_NAME_MAX) \
         -D LETTY_LOGIN_BASENAME=$(LETTY_LOGIN_BASENAME) \
         -D LETTY_LOGIN_PATH=$(LETTY_LOGIN_PATH) \
         -D LETTY_LOGIN_PROMPT=$(LETTY_LOGIN_PROMPT) \
         -D LETTY_UTMPX_USER=$(LETTY_UTMPX_USER) \
         -D LETTY_WTMP_PATH=$(LETTY_WTMP_PATH) \
         -D LETTY_CLEAR_SEQUENCE=$(LETTY_CLEAR_SEQUENCE) \
         -D LETTY_USE_UPDWTMP=$(LETTY_USE_UPDWTMP)

# ccomp without sse2
ifeq ($(CC),ccomp)
CFLAGS := $(CFLAGS) -fno-sse

# gcc and clang with additional flags
else
CFLAGS := $(CFLAGS) -Wall -Wextra -pedantic -Os -std=c99 -mpreferred-stack-boundary=2

# static linking for dietlibc and musl
ifeq ($(CC),musl-gcc)
CFLAGS := $(CFLAGS) -static
endif
endif

all: letty

letty: letty.c Makefile
	@echo LETTY_LOGIN_NAME_MAX=$(LETTY_LOGIN_NAME_MAX)
	@echo LETTY_LOGIN_BASENAME=$(LETTY_LOGIN_BASENAME)
	@echo LETTY_LOGIN_PATH=$(LETTY_LOGIN_PATH)
	@echo LETTY_LOGIN_PROMPT=$(LETTY_LOGIN_PROMPT)
	@echo LETTY_UTMPX_USER=$(LETTY_UTMPX_USER)
	@echo LETTY_WTMP_PATH=$(LETTY_WTMP_PATH)
	@echo LETTY_CLEAR_SEQUENCE=$(LETTY_CLEAR_SEQUENCE)
	@echo LETTY_USE_UPDWTMP=$(LETTY_USE_UPDWTMP)
	$(CC) $(CFLAGS) $@.c -o $@
	strip $@

man: letty.t2t
	txt2tags -o letty.8 letty.t2t

install:
	install -d $(DESTDIR)/sbin $(MANDIR)
	install letty $(DESTDIR)/sbin
	test -f letty.8.bz2 || bzip2 -k letty.8
	install letty.8.bz2 $(MANDIR)

uninstall:
	-rm -vf $(DESTDIR)/sbin/letty
	-rm -vf $(MANDIR)/letty.8.bz2

clean:
	@rm -vf letty letty.8.bz2 letty.o
